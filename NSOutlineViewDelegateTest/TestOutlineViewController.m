//
//  TestOutlineViewController.m
//  NSOutlineViewDelegateTest
//
//  Created by Christian Magnus on 12.04.12.
//  Copyright (c) 2012 iQser GmbH. All rights reserved.
//

#import "TestOutlineViewController.h"

@interface TestOutlineViewController ()

@end

@implementation TestOutlineViewController

@synthesize testOutlineView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
		treeRoot = [[NSTreeNode treeNodeWithRepresentedObject:nil] retain];
		
		NSTreeNode *top1 = [NSTreeNode treeNodeWithRepresentedObject:@"TOP1"];
		NSTreeNode *child11 = [NSTreeNode treeNodeWithRepresentedObject:@"Child1-1"];
		NSTreeNode *child12 = [NSTreeNode treeNodeWithRepresentedObject:@"Child1-2"];
		NSTreeNode *child13 = [NSTreeNode treeNodeWithRepresentedObject:@"Child1-3"];
		[top1.mutableChildNodes addObject:child11];
		[top1.mutableChildNodes addObject:child12];
		[top1.mutableChildNodes addObject:child13];
		[treeRoot.mutableChildNodes addObject:top1];
		
		NSTreeNode *top2 = [NSTreeNode treeNodeWithRepresentedObject:@"TOP2"];
		NSTreeNode *child21 = [NSTreeNode treeNodeWithRepresentedObject:@"Child2-1"];
		NSTreeNode *child22 = [NSTreeNode treeNodeWithRepresentedObject:@"Child2-2"];
		NSTreeNode *child23 = [NSTreeNode treeNodeWithRepresentedObject:@"Child2-3"];
		[top2.mutableChildNodes addObject:child21];
		[top2.mutableChildNodes addObject:child22];
		[top2.mutableChildNodes addObject:child23];
		[treeRoot.mutableChildNodes addObject:top2];
    }
    
    return self;
}

#pragma mark - outline view data source methods

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {
	if(item == nil) {
		return [treeRoot.childNodes count];
	}
	return [((NSTreeNode*)item).childNodes count];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
	return !((NSTreeNode*)item).isLeaf;
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
	if(item == nil) {
		return [treeRoot.childNodes objectAtIndex:index];
	} else {
		return [((NSTreeNode*)item).childNodes objectAtIndex:index];
	}
}

- (NSView *)outlineView:(NSOutlineView *)outlineView viewForTableColumn:(NSTableColumn *)tableColumn item:(id)item {
	NSTableCellView *retView;
	
	retView = [outlineView makeViewWithIdentifier:@"SampleRow" owner:self];
	[retView.textField setStringValue:((NSTreeNode*)item).representedObject];
	
	return retView;
}

#pragma mark - outline view delegate methods

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldShowOutlineCellForItem:(id)item {
	NSLog(@"shouldShowOutlineCellForItem");
	return !((NSTreeNode*)item).isLeaf;
}

- (void)outlineView:(NSOutlineView *)outlineView willDisplayOutlineCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item {
	NSLog(@"XXX willDisplayOutlineCell");
}

- (void)outlineView:(NSOutlineView *)theOutlineView willDisplayCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item {
	NSLog(@"XXX willDisplayCell");
}

@end
