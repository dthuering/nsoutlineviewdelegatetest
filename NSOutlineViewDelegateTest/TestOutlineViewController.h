//
//  TestOutlineViewController.h
//  NSOutlineViewDelegateTest
//
//  Created by Christian Magnus on 12.04.12.
//  Copyright (c) 2012 iQser GmbH. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "TestOutlineView.h"

@interface TestOutlineViewController : NSViewController <NSOutlineViewDataSource, NSOutlineViewDelegate> {
	NSTreeNode *treeRoot;
}

@property (nonatomic, assign) IBOutlet TestOutlineView *testOutlineView;

@end
