//
//  AppDelegate.m
//  NSOutlineViewDelegateTest
//
//  Created by Christian Magnus on 12.04.12.
//  Copyright (c) 2012 iQser GmbH. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize backgroundImage = _backgroundImage;
@synthesize targetView = _targetView;

- (void)dealloc
{
    [super dealloc];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// Insert code here to initialize your application
	
	NSImage *bgImage = [[NSImage alloc] initWithContentsOfFile:@"/Library/Desktop Pictures/Andromeda Galaxy.jpg"];
	self.backgroundImage.image = bgImage;
	[bgImage release];
	
	tvc = [[TestOutlineViewController alloc] initWithNibName:@"TestOutlineView" bundle:nil];
	[tvc.view setFrame:self.targetView.bounds];
	
	[self.targetView addSubview:tvc.view];
}

@end
