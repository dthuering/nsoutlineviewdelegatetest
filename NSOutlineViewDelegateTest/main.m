//
//  main.m
//  NSOutlineViewDelegateTest
//
//  Created by Christian Magnus on 12.04.12.
//  Copyright (c) 2012 iQser GmbH. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc, (const char **)argv);
}
