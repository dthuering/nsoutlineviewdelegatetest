//
//  AppDelegate.h
//  NSOutlineViewDelegateTest
//
//  Created by Christian Magnus on 12.04.12.
//  Copyright (c) 2012 iQser GmbH. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "TestOutlineViewController.h"

@interface AppDelegate : NSObject <NSApplicationDelegate> {
	TestOutlineViewController *tvc;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSImageView *backgroundImage;
@property (assign) IBOutlet NSView *targetView;

@end
