//
//  NSOutlineViewDelegateTestTests.m
//  NSOutlineViewDelegateTestTests
//
//  Created by Christian Magnus on 12.04.12.
//  Copyright (c) 2012 iQser GmbH. All rights reserved.
//

#import "NSOutlineViewDelegateTestTests.h"

@implementation NSOutlineViewDelegateTestTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in NSOutlineViewDelegateTestTests");
}

@end
